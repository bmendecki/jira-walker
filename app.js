"use strict";
const
    express = require('express'),
    fs = require('fs'),
    phantomjs = require('phantomjs-prebuilt'),
    bodyParser = require('body-parser'),
    urlencodedParser = bodyParser.urlencoded({ extended: false }),
    path = require('path'),
    spawn = require('child_process').spawn,
    app = express();

    app.get('/main', function(req,res){
    res.render('main.ejs');
   
});
app.post('/run', urlencodedParser, function(req,res){
    var url = req.body.url,
        login = req.body.login,
        pas = req.body.pas,
        childArgs = [
        path.join(__dirname, 'run.js'), url,login,pas
        ],
        child = spawn(phantomjs.path, childArgs);

    child.stdin.setEncoding('utf-8');
    child.stdout.pipe(process.stdout);
    child.on('close',function(code){
        console.log('child process exited with code: ' + code);

        fs.readFile('output.txt','utf8',(err,data)=>{
            if (err) throw err;

            res.render('result.ejs',{data : data});
        });
    });
});
app.use(express.static('public'));

app.get('*', function(req,res){
    res.redirect('/main');
});

app.listen(3000);
console.log('server listening on 3000');