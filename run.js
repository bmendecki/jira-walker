"use strict";
var page = require("webpage").create(),
    fs = require('fs'),
    system = require('system'),
    args = system.args,
    step1url = "https://jira.3bpo.com/login.jsp",
    step2url = args[1],
    step3url = step2url + "?page=com.atlassian.jira.plugin.system.issuetabpanels:worklog-tabpanel",
    login = args[2],
    pas = args[3];

var SaveWorkLogs = function (WorkLog) {
    fs.write('output.txt', WorkLog);
};

function evaluate(page, func) {
    var args = [].slice.call(arguments, 2);
    var fn = "function() { return (" + func.toString() + ").apply(this, " + JSON.stringify(args) + ");}";
    return page.evaluate(fn);
}

// page.onConsoleMessage = function(msg) {
//     console.log(msg);
// }
///////////////////////////////////////////////////////////////////////////////////////

setTimeout(function() {
    console.log("");
    console.log("### STEP 1: Load '" + step1url + "'");

    page.open(step1url,function(){

        evaluate(page, function(login,pas) {

            document.getElementById('login-form-username').value = login;
            document.getElementById('login-form-password').value = pas;
            document.getElementById('login-form-submit').click();
        }, login,pas);
    });
}, 0);

setTimeout(function() {
    console.log("");
    console.log("### STEP 2: Load '" + step2url );
    page.open(step2url,function(){
        setTimeout(function() {
        },3000)
    });
}, 3000);

setTimeout(function() {
    console.log("");
    console.log("### STEP 3: Load '" + step3url );
    page.open(step3url,function(){
        setTimeout(function() {
            var data = page.evaluate(function() {
                var WorkLog = [],
                isWorkLog = document.getElementById('issue_actions_container')
                    .innerHTML.indexOf('No work has yet been logged on this issue.');                    
                 
                 if(isWorkLog == -1) {   
                    
                        logs = document.getElementsByClassName('issue-data-block');
                        
    
                    for (var i = 0;i<logs.length;i++) {
                        var html = logs[i].innerHTML;
                        var pattern = /(alt="[a-z]+")/;
                        var pattern1 = /<dd\sid=\"wl-(\d+)-d"\sclass="worklog-duration">(.+)(\<\/dd\>)/;
                        var UserName = (html.match(pattern)[0]).slice(5,-1);
                        var WorkTime = (html.match(pattern1)[0]).slice(45,-5);
                        
                        WorkLog.push(UserName+"---" + WorkTime +"\n");
                    }                    
                 }
                else {
                 WorkLog.push('Nikt się nie logował :(');
                        
                }
                return WorkLog;
            
            },'data');
            
            SaveWorkLogs(data);

        },3000)
    });
}, 3000);

setTimeout(function() {
    console.log("");
    console.log("### STEP 4: Close page and shutdown (with a delay)");
    page.close();
    setTimeout(function(){
        phantom.exit();
    }, 100);
}, 10000);