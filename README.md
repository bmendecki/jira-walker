## Jira Walker

Funkcje

- wchodzi na task w Jirze
- pobiera czas pracy wszystkich userów na tym tasku
- wyświetla go w interfejsie

Użycie:

- uruchom node app.js
- wejdź na http://localhost:3000
- podaj task Jira, do którego masz uprawnienia
- podaj login, hasło i uruchom
- można obserwować output w konsoli

Technologie:
-  NodeJs,PhantomJS,